#!/bin/bash
# $1 = infile
# $2 = outfile
# $3 = sector
# $4 = bytes to skip

if [ "$#" -ne "4" ]; then
	echo "Usage: `basename $0` <boot.bin> <image> <sector> <skip>" >&2
	exit 1
fi

seek=$((512 * $3))
dd if=$1 of=$2 conv=notrunc bs=1 count=3 seek=$seek
dd if=$1 of=$2 conv=notrunc bs=1 skip=$4 seek=$(($seek + $4))