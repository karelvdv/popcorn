#!/bin/bash

if [ "$#" -ne "3" ]; then
	echo "Usage: `basename $0` <image_filename> <start> <blocks>" >&2
	exit 1
fi

OFFSET=$(( 512 * $2 ))

losetup -o$OFFSET /dev/loop0 $1
mkdosfs -F32 /dev/loop0 $3
losetup -d /dev/loop0