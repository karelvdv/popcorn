#!/bin/bash
# $1 = file
# $2 = hd size in mb

if [ "$#" -ne "2" ]; then
    echo "Usage: `basename $0` <image_filename> <disk size in mb>" >&2
    exit 1
fi

heads=16
sectors_per_track=63
bytes_per_sector=512
bytes_per_cylinder=$(( heads * (sectors_per_track + 1) * bytes_per_sector ))
cylinders=$(( ($2 * 1024 * 1024) / bytes_per_cylinder ))

#dd if=/dev/zero of=$1 bs=$bytes_per_cylinder count=$cylinders
bximage -q -hd -mode=flat -size=$2 $1

fdisk -u -C$cylinders -S$sectors_per_track -H$heads $1
