SRC=./src

.PHONY: stage0.bin stage1.bin

stage0.bin:
	$(MAKE) -C $(SRC) stage0mbr.bin

stage1.bin:
	$(MAKE) -C $(SRC) stage1fat.bin

floppy.img:
	mkdosfs -C "floppy.img" -n "POPCORN" -F16 1440

disk.img:
	./mkdisk.sh disk.img 64

bootdisk: stage0.bin stage1.bin disk.img
	dd if=stage0.bin of=disk.img conv=notrunc bs=1 count=446
	./cpsectorfat.sh stage1.bin disk.img 2048 90

bootfloppy: floppy.img stage1.bin
	./cpsectorfat.sh stage1.bin floppy.img 0 62

clean:
	rm -f *.bin