# POPCORN #

One day POPCORN will be the next-gen kernel that changed the world. Until then it is (will be) a small 64-bit hobby kernel that can be booted from a hard drive or USB stick.

POPCORN is designed for modern hardware and has no support for floppy drives or floppy drive emulation ("USB Legacy Support" in the BIOS).


## Boot loader ##
I have decided to write my own boot loader. Of course this involves reinventing the wheel, but it is mainly for fun and educational purposes. The boot loader is written in assembly and compiled with FASM.

The booting process is divided into three stages:

### stage0 ###
The sole purpose of stage0 is to load stage1 from a bootable partition. The stage0 binary is limited to 512 bytes, which may be just enough for a small boot manager but nothing else. If the boot drive has no support for partitions stage0 is skipped as there is no need for it.

### stage1 ###
The sole purpose of stage1 is to load stage2 from the file system. The stage1 binary is limited to 512 bytes, which is just enough space to read one particular file system. For this reason different file systems need their own stage1 binary.

### stage2 ###
The primary purpose of stage2 is to switch to 64-bit mode and load the POPCORN kernel. The stage2 binary is limited to 64 kilobytes, enough for a small OS in its own right. The secondary purpose is therefore to provide low-level diagnostics and services like fdisk, which are easier to do from 16-bit real mode.

