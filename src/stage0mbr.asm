; stage0 boot loader (mbr)
; the sole purpose of stage0 is
; to load the stage1 boot sector from
; a bootable partition to 0000:7c00
; and jump to it.

define MBR_TABLE_OFFSET  446
define MBR_ENTRY_LENGTH  16
define MBR_FLAG_BOOTABLE 0x80
define MBR_FLAG_LBA48    0x01
define MBR_ENTRIES_COUNT 4

; "unofficial" mbr with support for 48-bit lba addressing
virtual at di
	mbr.flag db ?  ; boot flags.
	mbr.sig1 db ?  ; signature-1
	mbr.stah dw ?  ; starting lba (47-32)
	mbr.sysi db ?  ; system id
	mbr.sig2 db ?  ; signature-2
	mbr.lenh dw ?  ; partition length (47-32)
	mbr.stal dd ?  ; starting lba (31-0)
	mbr.lenl dd ?  ; size in sectors (31-0)
end virtual

;------------------------------------------------------
; data passed on to stage1:
;  dl    boot device number.
;  ds:si pointer to the partition
;        table where stage1 was
;        loaded from.
use16
org 0x7c00
bootsec0:
	cli
	; [ds:si] = 0000:7c00 ; relocate from here,
	; [es:di] = 0000:0600 ; to there.
	; [ss:sp] = 0000:0600 ; stack grows down.
	xor si, si
	mov ds, si
	mov es, si
	mov fs, si
	mov gs, si
	mov ss, si
	mov si, 0x7c00
	mov di, 0x0600
	mov sp, di
	sti
	cld

	; copy 256 words from [ds:si] to [es:di].
	mov cx, 256
	repnz
	movsw

	; jump to finish relocation.
	jmp 0x0000:bootsec1
bootsec0_end:
;------------------------------------------------------

;------------------------------------------------------
; the origin is now different
org 0x0600 + (bootsec0_end - bootsec0)
bootsec1:
	; set video mode to 80x25 color.
	; also clears the screen.
	mov ax, 0x0003
	int 0x10

	; print a helpful message.
	mov si, msghello
	mov bl, 0x07
	call puts

	; detect lba addressing.
	mov ah, 0x41
	mov bx, 0x55aa
	int 0x13
	jc .die       ; pc is too ancient or
	test cx, 1    ; booting from floppy.
	jz .die

	; find bootable partition.
	mov cl, MBR_ENTRIES_COUNT
	mov di, 0x0600 + MBR_TABLE_OFFSET
@@: mov ch, byte [mbr.flag]
	test ch, MBR_FLAG_BOOTABLE
	jnz @f
	add di, MBR_ENTRY_LENGTH
	dec cl
	jnz @b
	jmp .die ; no bootable partition found!
@@:
	; validate that the partition has non-zero start and length.
	xor eax, eax
	xor ebx, ebx
	test ch, MBR_FLAG_LBA48
	jz @f
	mov ax, word [mbr.stah]  ; start  47-32
	mov bx, word [mbr.lenh]  ; length 47-32
@@: or eax, dword [mbr.stal] ; start  31-0
	or ebx, dword [mbr.lenl] ; length 31-0
	or ebx, eax
	jz .die

	; copy the first sector of the partition to 0x7c00.
	xor eax, eax
	test ch, MBR_FLAG_LBA48
	jz @f
	movsx eax, word [mbr.stah]
	; push data access packet
@@: push dword eax        ; lba high
	push dword [mbr.stal] ; lba low
	push word 0           ; segment
	push word 0x7c00      ; offset
	push word 1           ; sector count
	push word 0x0010      ; packet size
	; extended read
	mov si, sp
@@: mov ah, 0x42
	int 0x13
	jnc @f
	; reset and retry
	xor ah, ah
	int 0x13
	jmp @b
	; done
@@: add sp, 16

	; validate the boot signature.
	mov ax, word [0x7e00 - 2]
	cmp ax, 0xaa55
	jne .die

	; finally, jump to stage1!
	; parameters passed on:
	;  si = pointer to partition table
	;  dl = drive number
	mov si, di
	jmp 0x7c00
.die:
	mov si, msgerror
	mov bl, 0xcf
	call puts
@@: hlt
	jmp @b
;------------------------------------------------------


;------------------------------------------------------
; print zero-delimited string to screen.
; input:
;  bl    color
;  ds:si pointer to string
puts:
	pusha
	mov bh, 0x00
	mov ah, 0x0e
@@: lodsb
	or al, al
	jz @f
	int 0x10
	jmp @b
@@: popa
	ret
;------------------------------------------------------

align 1
msghello db 'POPCORN stage0 entered...', 13, 10, 0
msgerror db 'FATAL ERROR! Cannot load stage1!', 13, 10, 0

bootsec1_end:

;------------------------------------------------------
; pad to 512 bytes and add the boot signature.
times 510 - (bootsec0_end - bootsec0) - (bootsec1_end - bootsec1) db 0
dw 0xaa55