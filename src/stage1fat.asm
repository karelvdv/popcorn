; stage1 boot loader (fat32)
; the sole purpose of stage1 is
; to load stage2.bin from the file system
; to 8000:0000 and jump to it.

define MBR_SYSID_FAT32 0x0c

define CLUSTER_BUF_ADDR 0x0600

virtual at si
	mbr.flag db ?  ; boot flags.
	mbr.sig1 db ?  ; signature-1
	mbr.stah dw ?  ; starting lba (47-32)
	mbr.sysi db ?  ; system id
	mbr.sig2 db ?  ; signature-2
	mbr.lenh dw ?  ; partition length (47-32)
	mbr.stal dd ?  ; starting lba (31-0)
	mbr.lenl dd ?  ; size in sectors (31-0)
end virtual

struc fat32dir {
	name         db 11 dup ?
	attr         db ?
	ntres        db ?
	crttimetenth db ?
	crttime      dw ?
	crtdate      dw ?
	lstaccdate   dw ?
	fstclushi    dw ?
	wrttime      dw ?
	wrtdate      dw ?
	fstcluslo    dw ?
	filesize     dd ?
}

; data passed on to stage2:
;  dl    boot device number.
;  ds:si pointer to the partition
;        table where stage1 was
;        loaded from.
use16
org 0x7c00

boot_sector:
	jmp stage1
	nop

; bios parameter block
; must NOT be copied to the boot sector.
	bs_oemname          db 8 dup ?
	bpb_bytspersec      dw ?
	bpb_secperclus      db ?
	bpb_rsvdseccnt      dw ?
	bpb_numfats         db ?
	bpb_rootentcnt      dw ?
	bpb_totsec16        dw ?
	bpb_media           db ? ; hd=0xf8
	bpb_fatsz16         dw ?
	bpb_secpertrk       dw ?
	bpb_numheads        dw ?
	bpb_hiddsec         dd ?
	bpb_totsec32        dd ?
	bpb_fatsz32         dd ? ; fat sector count
	bpb_extflags        dw ?
	bpb_fsver           dw ?
	bpb_rootclus        dd ? ; root_dir_first_cluster
	bpb_fsinfo          dw ?
	bpb_bkbootsec       dw ?
	fat_begin_lba       dd ? ; bpb_reserved 0-3
	cluster_begin_lba   dd ? ; bpb_reserved 4-7
	sectors_per_cluster dd ? ; bpb_reserved 8-11
	bs_drvnum           db ?
	bs_reserved1        db ?
	bs_bootsig          db ?
	bs_volid            dd ?
	bs_vollab           db 11 dup ?
	bs_fstype           db  8 dup ?

kernel_fname db 'STAGE2  BIN' ; 11 bytes
mbr_entry dw 0

;--------------------------------------------------
; entry point
; input:
;  dl = drive number
;  si = ptr to partition table
stage1:
	cli
	xor sp, sp
	mov ds, sp
	mov es, sp
	mov fs, sp
	mov gs, sp
	mov ss, sp
	mov sp, 0x0600 ; [ss:sp] = 0000:0600.
	sti
	cld

	; remember input vars.
	mov byte [bs_drvnum], dl
	mov word [mbr_entry], si

	; print message
	push si
	mov si, _msghello
	mov bl, 0x07
	call puts
	pop si

	; check if partition type is fat32
	;mov ch, byte [mbr.sysi]
	;cmp ch, MBR_SYSID_FAT32
	;jne .die

	; check lba extensions present on boot drive.
	mov ah, 0x41
	mov bx, 0x55aa
	int 0x13
	jc .die    ; set if not present.
	test cx, 1 ; set if disk access packet supported.
	jz .die

	; fat_begin_lba = bpb_rsvdseccnt + mbr_lba_start
	movsx eax, word [bpb_rsvdseccnt]
	mov ebx, dword [mbr.stal]
	add ebx, eax
	mov dword [fat_begin_lba], ebx

	; cluster_begin_lba = fat_begin_lba + bpb_numfats * bpb_fatsz32
	movsx eax, byte [bpb_numfats]
	mov edx, dword [bpb_fatsz32]
	mul edx ; edx:eax = eax * edx
	add ebx, edx
	mov dword [cluster_begin_lba], ebx

	; sectors_per_cluster
	movsx eax, byte [bpb_secperclus]
	mov dword [sectors_per_cluster], eax

	; root cluster lba
	mov eax, dword [bpb_rootclus]
	call cluster_to_lba

	; load 1st root cluster
	push dword 0          ; lba high
	push dword [cluster_begin_lba] ; lba low
	push word 0           ; segment
	push word 0x1000      ; offset
	push word [sectors_per_cluster]           ; sector count
	push word 0x0010      ; packet size
	mov si, sp
	mov dl, [bs_drvnum]
@@: mov ah, 0x42
	int 0x13
	jnc @f
	xor ah, ah
	int 0x13
	jmp @b
@@: add sp, 16

	; es:di destination

	jmp $
	; finally, far jump to stage2 @ 8000:0000!
	jmp 0x8000:0000

;--------------------------------------------------
; fatal error.
.die:
	mov si, _msgerror
	mov bl, 0xcf
	call puts
@@: hlt
	jmp @b
;--------------------------------------------------


;--------------------------------------------------
; convert cluster to lba address.
; lba_addr = cluster_begin_lba + (cluster_number - 2) * sectors_per_cluster
; input:
;  eax cluster_number
; output:
;  eax lba_addr
cluster_to_lba:
	push edx
	sub eax, 2
	mov edx, dword [sectors_per_cluster]
	mul edx ; edx:eax = eax * edx
	add eax, dword [cluster_begin_lba]
	pop edx
	ret
;--------------------------------------------------

; input:
;  cx   nr of sectors to read
;  eax  lba of first sector
;  es:di destination
read_sectors:
	push dword 0      ; lbah
	push dword eax    ; lbal
	push  word es     ; segm
	push  word di     ; offs
	push  word cx     ; sctr
	push  word 0x0010 ; size

	mov si, sp
	mov dl, byte [bs_drvnum]
	mov ah, 0x42
	int 0x13
	jc stage1.die

	add si, 16
	pop si
	pop dx
	pop ax
	ret


;--------------------------------------------------
; print a single character.
; input:
;  bl color
;  al character
putchar:
	pusha
	mov bh, 0x00
	mov ah, 0x0e
	int 0x10
	popa
	ret
;--------------------------------------------------


;--------------------------------------------------
; print zero-delimited string to screen.
; input:
;  bl    color
;  ds:si pointer to string
puts:
	pusha
	mov bh, 0x00
	mov ah, 0x0e
@@: lodsb
	or al, al
	jz @f
	int 0x10
	jmp @b
@@: popa
	ret
;--------------------------------------------------


; input:
;  es:si a
;  ds:di b
;  cx count
; output:
;  cx == 0 if   equal
;  cx  > 0 if unequal
memcmp:
	repe cmpsb
	ret

;--------------------------------------------------
; disk address packet
align 4
dapack:
	.size db 16     ; size of dap packet.
	      db 0      ; always 0.
	.sctr dw 1      ; number of sectors to transfer.
	.offs dw 0      ; destination 16-bit offset.
	.segm dw 0      ; destination 16-bit segment.
	.lbal dd 0      ; lba sector to start reading from.
	.lbah dd 0      ; lba sector upper 48-bits.
;--------------------------------------------------



;--------------------------------------------------
align 1

; constants
_msghello db 'POPCORN stage1 entered...', 13, 10, 0
_msgerror db 'FATAL ERROR! Boot cannot continue!', 13, 10, 0

times 510-$+boot_sector db 0
dw 0xaa55
